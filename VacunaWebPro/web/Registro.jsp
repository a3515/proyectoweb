
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Registro de Vacunación</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS only -->
        <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script src=" href='https://cdn.bootcss.com/sweetalert/1.1.3/sweetalert.min.js">https://cdn.bootcss.com/sweetalert/1.1.3/sweetalert.min.js"></script>
    </head>
    
    <body>
        <nav class="navbar navbar-light bg-primary">
            <a class="navbar-brand" href="#" style="color:white">Registro de Datos</a>
        </nav>
        
        <div class="container">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-4">Vacuna Covid-19</h1>
                    <p class="lead">Cuando sea tu momento #Vacúnate</p>
                </div>
            </div>    
            <h2>Datos Generales</h2>
            <br> 
              
                <div class="container" >
                    <form action="" method="post">
 
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblNacionalidad" class="form-label">Nacionalidad *</label>
                                    <select class="form-control" name="InputNnacionalidad" readonly="1" >
                                        <option>guatemalteco</option>
                                    </select>
                                </div>
                                <!-- segunda columna -->
                                <div class="col-md-6">
                                    <label for="lblCUI" class="form-label">C.U.I.*</label>
                                    <input  class="form-control" type="text" name="InputCUI" placeholder="2054826960201">
                                </div>
                            </div>
                        </div>
                        
                        <!-- segunda fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-4">
                                    <label for="lblPrimerNombre" class="form-label">Nombres *</label>
                                    <input  class="form-control" type="text" name="InputPrimerNombre" placeholder="Silvestra">
                                </div>
                                <!-- segunda columna -->
                                <div class="col-md-4">
                                    <label for="lblSegundoNombre" class="form-label">&nbsp;</label>
                                    <input  class="form-control" type="text" name="InputSegundoNombre" placeholder="Aurora">
                                </div>
                                <!-- tercera columna -->
                                <div class="col-md-4">	
                                    <label for="lblTercerNombre" class="form-label"> &nbsp;</label>
                                    <input  class="form-control" type="text" name="InputTercerNombre" placeholder="Tercer Nombre">
                                </div>
                            </div>
                        </div>

                        <!-- tercera fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblPrimerApellido" class="form-label">Apellidos *</label>
                                    <input  class="form-control" type="text" name="InutPrimerApellido" placeholder="Alvarado">
                                </div>
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblSegundoApellido" class="form-label">&nbsp;</label>
                                    <input  class="form-control" type="text" name="InputSegundoApellido" placeholder="De León">
                                </div>
                            </div>
                        </div>
                        
                        <!-- cuarta fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-4">
                                    <label for="lblSexo" class="form-label">Sexo *</label>
                                    <select class="form-control" name="OpSexo">
                                        <option>Mujer</option>
                                        <option>Hombre</option>
                                    </select>
                                </div>
                                <!-- segunda columna -->
                                <div class="col-md-4">
                                    <label for="lblFechaNacimiento" class="form-label">Fecha de Ncimiento</label>
                                    <input  class="form-control" type="text" name="InputFechaNacimiento" placeholder="Aurora">
                                </div>
                                <!-- tercera columna -->
                                <div class="col-md-4">	
                                    <label for="lblEscolaridad" class="form-label">Escolaridad</label>
                                    <select class="form-control" name="OpEscolaridad" required="" >
                                        <option>Pre-Primaria</option>
                                        <option>Primaria</option>
                                        <option>Básicos</option>
                                        <option>Diversificado</option>
                                        <option>Universidad</option>
                                        <option>No Aplica</option>
                                        <option>No Indica</option>
                                        <option>Otro</option>
                                        <option>Ninguno</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Quinta fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-4">     
                                    <label for="lblDptoResidencia" class="form-label">Departamento Residencia *</label>
                                    <input  class="form-control" type="text" name="InputDptoResidencia" placeholder="Alvarado" required="">
                                </div>
                                <!--tercera columna -->
                                <div class="col-md-4">
                                    <label for="lblMuniResidencia" class="form-label">Municipio Residencia *</label>
                                    <input  class="form-control" type="text" name="InputMuniResidencia" placeholder="De León" required="" >
                                </div>
                                 <div class="col-md-4">
                                    <label for="lblZona" class="form-label">Zona </label>
                                     <select class="form-control" name="OpZona" required="">
                                        <option>Zona 1</option>
                                     </select>
                                </div>
                            </div>
                        </div>
                        
                        <!-- sexta fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-12">     
                                    <label for="lblDireccionResidencia" class="form-label">Dirección de Residencia *</label>
                                    <input  class="form-control" type="text" name="InputDireResidencia" placeholder="Alvarado" required="">
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <!-- septima fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                           <div class="col-md-6">
                                    <label for="lblPueblo" class="form-label">Pueblo *</label>
                                    <select class="form-control" name="OpPueblo" required="">
                                        <option>Garifuna</option>
                                        <option>Ladino/Mestizo</option>
                                        <option>Maya</option>
                                        <option>Xinca</option>    
                                    </select>
                                </div>
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblComunidadLing" class="form-label">Comunidad Linguistica *</label>
                                    <select class="form-control" name="OpComunidad" required="">
                                        <option>Español/Castellano</option>
                                        <option>Achi</option>
                                        <option>Akateko</option>
                                        <option>Chorti</option>
                                        <option>Chuj</option>
                                        <option>Itza</option>
                                        <option>Ixil</option>
                                        <option>Jakalteko</option>
                                        <option>Qanjobal</option>
                                        <option>Kaqchikel</option>                                       
                                        <option>Kiche</option>
                                        <option>Man</option>
                                        <option>Mopan</option>
                                        <option>Poqomam</option>                                        
                                        <option>Poqomchi</option>                                       
                                        <option>Qeqchi</option>
                                        <option>Sakapulteko</option>
                                        <option>Sipakapense</option>
                                        <option>Tektiteko</option>
                                        <option>Tzutujil</option>
                                        <option>Uspanteko</option>
                                        <option>Xinca</option>
                                        <option>Garifuna</option>     
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <!-- octava fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblCorreo" class="form-label">Correo Electónico</label>
                                    <input  class="form-control" type="text" name="InputCorreo" placeholder="Correo Electrónico">
                                </div>
                                <!-- segunda columna -->
                                <div class="col-md-4">
                                    <label for="lblcelular" class="form-label">Teléfono Celular</label>
                                    <input  class="form-control" type="text" name="InputCelular" placeholder="Teléfono Celular" required="">
                                </div>
                                <!--tercera columna -->
                                <div class="col-md-2">
                                    <label for="lblLinea" class="form-label">Linea</label>
                                        <select class="form-control" name="OpLinea" id="lineaTel" required="">
                                        <option>Claro</option>
                                        <option>Tigo</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <!-- octava fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                </div>
                                <!--segunda columna -->
                                <div class="col-md-6">
                                    <label for="lblTelefono" class="form-label">Confirmar Teléfono</label>
                                    <input  class="form-control" type="text" name="InputTelefono" placeholder="Repetir Teléfono Célular" required="">
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <!-- novena fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblDiscapacidad" class="form-label">Discapacidad *</label>
                                    <select class="form-control" name="OpDiscapacidad" required="" >
                                        <option>Auditiva</option>
                                        <option>Fisica</option>
                                        <option>Mental</option>
                                        <option>Multiple</option>
                                        <option>No Aplica</option>
                                        <option>Otro</option>
                                        <option>Visual</option> 
                                    </select>
                                </div>
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblEnfermedadCronica" class="form-label">Enfermedad Crónica *</label>
                                    <select class="form-control" name="OpEnfermedadCronica" required="">
                                        <option>Diabet mellitus</option>
                                        <option>Enfermedad pulmonary cronica</option>
                                        <option>Enfermedad renal croica</option>
                                        <option>Enfermedades cariovasculares</option>
                                        <option>Enfermedades cerebrobasculares</option>
                                        <option>Hipertension arterial</option>
                                        <option>Inmunosupresion arterial</option>                          
                                        <option>Inmunosupresion-uso-inmunosupresores</option> 
                                        <option>Inmunosupresion-VIH</option> 
                                        <option>Ninguno</option> 
                                        <option>Obesidad</option> 
                                        <option>otra</option> 
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br> <br>
                        
                        <!-- decima fila -->
                     <div class="form-group">
                            <div class="row">
                               
                                <div class="col-md-2">
                                    <label for="lblAfiliacion" class="form-label">Tiene afiliación IGSS</label>
                                    <select class="form-control" name="OpAfiliacion" required="">
                                        <option>Si</option>
                                        <option>No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

 
                        <br>
                        <h2>Puesto de Vacunación</h2>
                        <br><br>

                        
                        <!-- decima primera fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-6">     
                                    <label for="lblDpto" class="form-label">Departamento *</label>
                                     <select class="form-control" name="OpDpto" required="">
                                        <option>Guatemala</option>
                                     </select>
                                </div>
                                <!--tercera columna -->
                                <div class="col-md-6">
                                    <label for="lblMunicipio" class="form-label">Municipio *</label>
                                       <select class="form-control" name="OpMunicipio" required="">
                                            <option>Guatemala</option>
                                       </select>
                                </div>
                            </div>
                        </div>


                        <!-- decima segunda fila -->
                        <div class="form-group">
                            <div class="row">
                                <!-- primera columna -->
                                <div class="col-md-12">     
                                    <label for="lblPuestoVacunacion" class="form-label">Puesto de Vacunación *</label>
                                    <input  class="form-control" type="text" name="InputPuestoVacunacion" placeholder="">
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Registrar" onclick="EventoAlert()"/>
                            <script>
                                        function EventoAlert() {
                                                Swal.fire(
                                            'Informaciòn Guardad con éxito.',
                                            'El Centro de Salud más cercano se estará comunicando con usted para indicar el día, lugar y hora en que será vacunado',
                                            'success'
                                          )
                                        }
                            </script>
                    </form>
                    <br><br>
                    <br><br>
                </div>
        </div>
    </body>
</html>
