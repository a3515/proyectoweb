# ProyectoWEB
UNIVERSIDAD MARIANO GALVEZ DE GUATEMALA
PROGRAMACION II		 
PROYECTO 2

## Getting started


## Add your files


## Integrate with your tools
	JDK 11 y 8
	NetBeans 12.04
	Tomcat 10
	
## Collaborate with your team
ANIBAL ROBERTO GOMEZ MORALES 	0910-20-19042
LEONEL ALEJANDRO ZAMORA			0910-20- 23685

## Test and Deploy
http://localhost:8080/VacunaWebPro/Registro.jsp


## Description
El proyecto consiste en replicar un sistema de ingreso de personas para vacunación, el cuál debe validar correctamente todos los datos ingresados y posteriormente almacenarlos en una base de datos Oracle.


## Badges

## Installation

## Usage

NetBeans IDE 
Product Version: Apache NetBeans IDE 12.4
Java: 1.8.0_251; Java HotSpot(TM) 64-Bit Server VM 25.251-b08
Runtime: Java(TM) SE Runtime Environment 1.8.0_251-b08
System: Windows 10 version 10.0 running on amd64; Cp1252; es_ES (nb)

Tomcat 
Apache Tomcat, el servidor web usado en tradicionalmente para proyectos Java por su implementación de servlets o páginas JSP, es otra de las aplicaciones que podemos desplegar fácilmente en los Servidores Cloud de Arsys desde el Catálogo de Aplicaciones.
Apache Tomcat (o, sencillamente, Tomcat) es un contenedor de servlets que se puede usar para compilar y ejecutar aplicaciones web realizadas en Java. Implementa y da soporte tanto a servlets como a páginas JSP (Java Server Pages) o Java Sockets. Además, Tomcat es compatible con otras tecnologías como Java Expression Language y Java WebSocket, del ecosistema Java. 
Tomcat puede funcionar de manera autónoma como motor de aplicaciones web desarrolladas con Java, aunque habitualmente se usa en combinación con otros productos como el servidor web Apache, para dar un mayor soporte a tecnologías y aumentar sus características.
## Support
ANIBAL ROBERTO GOMEZ MORALES 	
LEONEL ALEJANDRO ZAMORA			

## Authors and acknowledgment
ANIBAL ROBERTO GOMEZ MORALES 	
LEONEL ALEJANDRO ZAMORA		

## License
Copyright. 

## Project status
Proyecto Estable